package ru.itigris.migrateusers.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.lang.Nullable;

import java.util.Objects;

/**
 * Структура для сбора данных о старом пользователе.
 * <p>
 * Данный класс может быть расширен, если в проекте используются дополнительные аттрибуты для пользователей.
 */
public class OldUser {

    @NotNull
    protected Long id;

    @NotNull
    protected String email;

    @NotNull
    protected Boolean enabled;

    @Nullable
    protected String password;

    protected OldUser(@NotNull Long id, @NotNull String email, @NotNull Boolean enabled, @Nullable String password) {
        Objects.requireNonNull(enabled);
        if (enabled && password == null) {
            throw new IllegalArgumentException("Пароль должен присутствовать, если юзер активирован!");
        }
        this.id = Objects.requireNonNull(id);
        this.email = Objects.requireNonNull(email);
        this.enabled = enabled;
        this.password = password;
    }

    @NotNull
    public Long getId() {
        return id;
    }

    @NotNull
    public String getEmail() {
        return email;
    }

    @NotNull
    public Boolean isEnabled() {
        return enabled;
    }

    @Nullable
    public String getPassword() {
        return password;
    }

    @SuppressWarnings("unchecked")
    public static class Builder<T extends Builder<T>> {
        protected Long id;
        protected String email;
        protected Boolean enabled;
        protected String password;

        protected Builder() {
        }

        public T withId(Long id) {
            this.id = id;
            return (T) this;
        }

        public T withEmail(String email) {
            this.email = email;
            return (T) this;
        }

        public T withEnabled(Boolean enabled) {
            this.enabled = enabled;
            return (T) this;
        }

        public T withPassword(String password) {
            this.password = password;
            return (T) this;
        }

        public OldUser build() {
            return new OldUser(id, email, enabled, password);
        }
    }

    public static Builder builder() {
        return new Builder<>();
    }

    @Override
    public String toString() {
        return "OldUser{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", enabled=" + enabled +
                ", password='" + password + '\'' +
                '}';
    }
}
