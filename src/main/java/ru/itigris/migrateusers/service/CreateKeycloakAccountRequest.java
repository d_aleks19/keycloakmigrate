package ru.itigris.migrateusers.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

/**
 * Запрос на создание пользователя в Keycloak.
 *
 * @see KeycloakSdk#createUser(CreateKeycloakAccountRequest)
 */
public class CreateKeycloakAccountRequest {

    @NotNull
    private final String email;

    @NotNull
    private final Boolean enabled;

    @Nullable
    private final String hashedPassword;

    @NotNull
    private final String role;

    private CreateKeycloakAccountRequest(@NotNull String email, @NotNull Boolean enabled,
                                         @Nullable String hashedPassword, @NotNull String role) {
        Objects.requireNonNull(enabled);
        if (enabled && hashedPassword == null) {
            throw new IllegalArgumentException("Пароль должен присутствовать, если юзер активирован!");
        }
        this.email = Objects.requireNonNull(email);
        this.enabled = enabled;
        this.hashedPassword = hashedPassword;
        this.role = Objects.requireNonNull(role);
    }

    @NotNull
    public String getEmail() {
        return email;
    }

    @NotNull
    public String getRole() {
        return role;
    }

    @NotNull
    public Boolean isEnabled() {
        return enabled;
    }

    @Nullable
    public String getHashedPassword() {
        return hashedPassword;
    }

    public static final class Builder {
        private String email;
        private Boolean enabled;
        private String hashedPassword;
        private String role;

        private Builder() {
        }

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder withEnabled(Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        public Builder withHashedPassword(String hashedPassword) {
            this.hashedPassword = hashedPassword;
            return this;
        }

        public Builder withRole(String role) {
            this.role = role;
            return this;
        }

        public CreateKeycloakAccountRequest build() {
            return new CreateKeycloakAccountRequest(email, enabled, hashedPassword, role);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return "CreateKeycloakAccountRequest{" +
                "email='" + email + '\'' +
                ", role='" + role + '\'' +
                ", enabled=" + enabled +
                ", hashedPassword='" + hashedPassword + '\'' +
                '}';
    }
}
