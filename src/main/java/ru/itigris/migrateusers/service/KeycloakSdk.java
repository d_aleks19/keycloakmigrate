package ru.itigris.migrateusers.service;

import org.jetbrains.annotations.NotNull;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import javax.ws.rs.core.Response;
import java.sql.Types;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Факад для работы с REST-API админкой Keycloak.
 */
@Service
public class KeycloakSdk {

    private final Logger logger = LoggerFactory.getLogger(KeycloakSdk.class);

    private final JdbcTemplate keycloakJdbcTemplate;
    private final TransactionTemplate transactionTemplate;
    private final RealmResource realm;

    public KeycloakSdk(@Qualifier("keycloakJdbcTemplate") JdbcTemplate keycloakJdbcTemplate,
                       @Qualifier("keycloakTransactionManager") PlatformTransactionManager transactionManager,
                       RealmResource realm) {
        this.keycloakJdbcTemplate = keycloakJdbcTemplate;
        this.transactionTemplate = new TransactionTemplate(transactionManager);
        this.transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        this.transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        this.realm = realm;
    }

    /**
     * Добавляет пользователю указанную роль.
     *
     * @param accountId ID аккаунта
     * @param role      роль (как она задана в Keycloak)
     */
    private void addRealmRole(@NotNull final String accountId, final @NotNull String role) {
        Objects.requireNonNull(accountId);
        Objects.requireNonNull(role);

        logger.debug("Добавление роли пользователю. ID аккаунта: {}, Роль: {}", accountId, role);

        final RoleRepresentation roleRepresentation = realm.roles().get(role).toRepresentation();
        realm.users().get(accountId).roles().realmLevel().add(List.of(roleRepresentation));
    }

    /**
     * Устанавливает новый пароль пользователя.
     * <p>
     * На наших проектах везде использовался дефолтный {@code BCryptPasswordEncoder}. Поэтому подразумевается, что
     * для пароля было совершено 2^10 итераций хэширования, что является дефолтным значением для
     * {@code BCryptPasswordEncoder}.
     *
     * @param accountId      ID аккаунта
     * @param hashedPassword хэшированный пароль
     */
    private void updatePassword(@NotNull final String accountId, @NotNull final String hashedPassword) {
        Objects.requireNonNull(accountId);
        Objects.requireNonNull(hashedPassword);

        logger.debug("Обновление пароля пользователя. ID аккаунта: {}, Хэшированный пароль: {}",
                accountId, hashedPassword);

        final String credentialData = "{\"value\":\"%s\",\"salt\":\"\"}";

        transactionTemplate.executeWithoutResult(status -> {
            keycloakJdbcTemplate.update("insert into credential " +
                    "(id, salt, type, user_id, created_date, user_label, secret_data, credential_data, priority) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", ps -> {
                ps.setString(1, UUID.randomUUID().toString().toLowerCase());
                ps.setNull(2, Types.VARBINARY);
                ps.setString(3, "password");
                ps.setString(4, accountId);
                ps.setLong(5, Instant.now().toEpochMilli());
                ps.setNull(6, Types.VARCHAR);
                ps.setString(7, String.format(credentialData, hashedPassword));
                ps.setString(8, "{\"hashIterations\":-1,\"algorithm\":\"bcrypt\"}");
                ps.setInt(9, 10);
            });
        });
    }

    /**
     * Создает юзера с указанной ролью.
     *
     * @param request запрос на создание аккаунта
     * @return ID созданного юзера
     */
    @NotNull
    public String createUser(@NotNull CreateKeycloakAccountRequest request) {
        Objects.requireNonNull(request);

        logger.debug("Создание пользователя в Keycloak. Email: {}", request.getEmail());

        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setEmail(request.getEmail());
        userRepresentation.setUsername(request.getEmail());
        userRepresentation.setEnabled(request.isEnabled());
        Response userCreateResponse = realm.users().create(userRepresentation);
        if (userCreateResponse.getStatus() != HttpStatus.CREATED.value()) {
            throw new RuntimeException("Ошибка создания пользователя. Статус: " + userCreateResponse.getStatus());
        }
        String createdUserId = CreatedResponseUtil.getCreatedId(userCreateResponse);
        userCreateResponse.close();

        logger.debug("Пользователь создан в Keycloak. Email: {}, ID: {}",
                request.getEmail(), createdUserId);

        addRealmRole(createdUserId, request.getRole());
        if (request.getHashedPassword() != null) {
            updatePassword(createdUserId, request.getHashedPassword());
        }
        return createdUserId;
    }

    public void addGroup(@NotNull String accountId, @NotNull String group) {
        realm.users().get(accountId).joinGroup(realm.getGroupByPath(group).getId());
    }

    /**
     * Удаляет всех пользователей с реалма, кроме админа (админ требуется для управления реалмом).
     */
    public void deleteAllUsers() {
        logger.debug("Удаление всех пользователей с реалма");
        int deletedCount = 0;
        for (UserRepresentation user : realm.users().list()) {
            if ("admin".equals(user.getUsername())) {
                logger.debug("Админ-пользователь не удаляется. ID: {}", user.getId());
                continue;
            }
            logger.debug("Удаление пользователя с ID {}", user.getId());
            realm.users().delete(user.getId());
            ++deletedCount;
        }
        logger.debug("Пользователи удалены с реалма. Количество удаленных пользователей: {}", deletedCount);
    }

    public void enableUser(@NotNull String id) {
        Objects.requireNonNull(id);
        UserResource userResource = realm.users().get(id);
        UserRepresentation userRepresentation = userResource.toRepresentation();
        userRepresentation.setEnabled(true);
        userResource.update(userRepresentation);
    }
}
