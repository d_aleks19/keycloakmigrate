package ru.itigris.migrateusers.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Предоставляет интерфейс для миграций.
 * <p>
 * Для имплементации миграций конкретного проекта необходимо унаследоваться от данного класса и реализовать
 * метод {@link #doMigrate()}.
 */
public abstract class MigrateService {

    private final Logger logger = LoggerFactory.getLogger(MigrateService.class);

    protected final KeycloakSdk keycloakSdk;

    public MigrateService(KeycloakSdk keycloakSdk) {
        this.keycloakSdk = keycloakSdk;
    }

    /**
     * Имплементируйте данный метод.
     */
    protected abstract void doMigrate();

    private void rollback() {
        keycloakSdk.deleteAllUsers();
    }

    public void migrate() {
        logger.info("Миграции начались");

        try {
            doMigrate();
        } catch (Exception ex) {
            logger.error("Произошла ошибка во время выполнения миграций", ex);
            logger.error("Производится Rollback миграций...");
            rollback();
            logger.error("Rollback успешно произведен");
            System.exit(0);
        }

        logger.info("Миграции успешно завершены");
        System.exit(0);
    }
}
