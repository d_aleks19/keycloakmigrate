package ru.itigris.migrateusers.service.cooper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;
import ru.itigris.migrateusers.service.CreateKeycloakAccountRequest;
import ru.itigris.migrateusers.service.KeycloakSdk;
import ru.itigris.migrateusers.service.MigrateService;
import ru.itigris.migrateusers.service.OldUser;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Имплементация миграций для CooperVision.
 */
@ConditionalOnProperty(prefix = "migrate", name = "target", havingValue = "COOPER")
@Service
public class CooperMigrateService extends MigrateService {

    private final Logger logger = LoggerFactory.getLogger(CooperMigrateService.class);

    private final JdbcTemplate jdbcTemplate;
    private final TransactionTemplate transactionTemplate;

    public CooperMigrateService(KeycloakSdk keycloakSdk,
                                @Qualifier("projectJdbcTemplate") JdbcTemplate jdbcTemplate,
                                @Qualifier("projectTransactionManager") PlatformTransactionManager transactionManager) {
        super(keycloakSdk);
        this.jdbcTemplate = jdbcTemplate;
        this.transactionTemplate = new TransactionTemplate(transactionManager);
        this.transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        this.transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
    }

    @Override
    protected void doMigrate() {
        logger.info("Сбор информации о старых пользователях...");
        List<OldUser> oldUsers = jdbcTemplate.query("select u.id as id, u.user_account_id, ua.email as email, " +
                        "ua.password as password, ua.enabled as enabled " +
                        "from users u " +
                        "inner join user_accounts ua ON u.user_account_id=ua.id",
                (rs, rowNum) -> OldUser.builder()
                        .withId(rs.getLong("id"))
                        .withEmail(rs.getString("email"))
                        .withPassword(rs.getString("password"))
                        .withEnabled(rs.getBoolean("enabled"))
                        .build()
        );
        logger.info("Старые пользователи собраны. Количество: {}, Пользователи: {}", oldUsers.size(), oldUsers);

        logger.info("Теперь создаем пользователей в Keycloak...");
        Map<Long, String> usersKeycloakId = new HashMap<>(oldUsers.size());
        for (OldUser oldUser : oldUsers) {
            String keycloakId = keycloakSdk.createUser(CreateKeycloakAccountRequest.builder()
                    .withEmail(oldUser.getEmail().strip())
                    .withEnabled(oldUser.isEnabled())
                    .withRole("buyer")
                    .withHashedPassword(oldUser.getPassword())
                    .build());

            usersKeycloakId.put(oldUser.getId(), keycloakId);
        }
        logger.info("Пользователи в Keycloak реалме созданы. [Old User ID]-[Keycloak User ID] маппинг: {}",
                usersKeycloakId);

        BatchPreparedStatementSetter pss = new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Long oldUserId = oldUsers.get(i).getId();
                ps.setString(1, usersKeycloakId.get(oldUserId));
                ps.setString(2, String.valueOf(oldUserId));
            }

            @Override
            public int getBatchSize() {
                return oldUsers.size();
            }
        };

        logger.info("Обновление данных в БД проекта в соответствии с новыми ID пользователей...");
        transactionTemplate.executeWithoutResult(status -> {
            logger.trace("delete from 'reset_password_tokens'");
            jdbcTemplate.batchUpdate("delete from reset_password_tokens");
            logger.trace("update 'orders'");
            jdbcTemplate.batchUpdate("update orders set user_id = ? where user_id = ?", pss);
            logger.trace("update 'orders_changelog'");
            jdbcTemplate.batchUpdate("update orders_changelog set modifier_id = ? " +
                    "where modifier_type = 'USER' and " +
                    "modifier_id = ?", pss);
            jdbcTemplate.batchUpdate("update orders_changelog " +
                    "set modifier_id = 'distributor' " +
                    "where modifier_type = 'DISTRIBUTOR'");
            logger.trace("update 'users_legal_entities'");
            jdbcTemplate.batchUpdate("update users_legal_entities " +
                    "set user_id = ? where user_id = ?", pss);
        });
        logger.info("Данные в БД проекта обновлены");
    }
}
