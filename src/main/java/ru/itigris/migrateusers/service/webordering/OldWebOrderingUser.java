package ru.itigris.migrateusers.service.webordering;

import org.jetbrains.annotations.NotNull;
import org.springframework.lang.Nullable;
import ru.itigris.migrateusers.service.OldUser;

import java.util.Objects;

public class OldWebOrderingUser extends OldUser {

    @NotNull
    private String role;

    private OldWebOrderingUser(@NotNull Long id, @NotNull String email, @NotNull Boolean enabled,
                               @Nullable String password, @NotNull String role) {
        super(id, email, enabled, password);
        this.role = Objects.requireNonNull(role);
    }

    @NotNull
    public String getRole() {
        return role;
    }

    public static final class Builder extends OldUser.Builder<Builder> {
        private String role;

        private Builder() {
        }

        public Builder withRole(String role) {
            this.role = role;
            return this;
        }

        public OldWebOrderingUser build() {
            return new OldWebOrderingUser(id, email, enabled, password, role);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return "OldWebOrderingUser{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", role='" + role + '\'' +
                ", enabled=" + enabled +
                ", password='" + password + '\'' +
                '}';
    }
}
