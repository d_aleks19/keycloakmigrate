package ru.itigris.migrateusers.service.webordering;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;
import ru.itigris.migrateusers.service.CreateKeycloakAccountRequest;
import ru.itigris.migrateusers.service.KeycloakSdk;
import ru.itigris.migrateusers.service.MigrateService;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Имплементация миграций для WebOrdering.
 */
@ConditionalOnProperty(prefix = "migrate", name = "target", havingValue = "WEB-ORDERING")
@Service
public class WebOrderingMigrateService extends MigrateService {

    private final Logger logger = LoggerFactory.getLogger(WebOrderingMigrateService.class);

    private final JdbcTemplate jdbcTemplate;
    private final TransactionTemplate transactionTemplate;

    public WebOrderingMigrateService(KeycloakSdk keycloakSdk,
                                     @Qualifier("projectJdbcTemplate") JdbcTemplate jdbcTemplate,
                                     @Qualifier("projectTransactionManager") PlatformTransactionManager transactionManager) {
        super(keycloakSdk);
        this.jdbcTemplate = jdbcTemplate;
        this.transactionTemplate = new TransactionTemplate(transactionManager);
        this.transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        this.transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
    }

    @Override
    protected void doMigrate() {
        logger.info("Сбор информации о старых пользователях...");
        List<OldWebOrderingUser> oldUsers = jdbcTemplate.query(
                "select u.id as id, " +
                        "u.role as role, " +
                        "ua.email as email, " +
                        "ua.password as password, " +
                        "ua.enabled as enabled " +
                        "from users u " +
                        "inner join user_accounts ua ON u.user_account_id=ua.id::varchar",
                (rs, rowNum) -> OldWebOrderingUser.builder()
                        .withId(rs.getLong("id"))
                        .withEmail(rs.getString("email"))
                        .withRole(rs.getString("role"))
                        .withEnabled(rs.getBoolean("enabled"))
                        .withPassword(rs.getString("password"))
                        .build()
        );
        logger.info("Старые пользователи собраны. Количество: {}, Пользователи: {}", oldUsers.size(), oldUsers);

        logger.info("Теперь создаем пользователей в Keycloak...");
        Map<Long, String> userKeycloakIdByOldId = new HashMap<>(oldUsers.size());
        for (OldWebOrderingUser oldUser : oldUsers) {
            String keycloakId = keycloakSdk.createUser(CreateKeycloakAccountRequest.builder()
                    .withEmail(oldUser.getEmail().strip())
                    .withEnabled(oldUser.isEnabled())
                    .withRole(oldUser.getRole().toLowerCase())
                    .withHashedPassword(oldUser.getPassword())
                    .build());
            keycloakSdk.addGroup(keycloakId, "/show_prices");

            userKeycloakIdByOldId.put(oldUser.getId(), keycloakId);
        }
        logger.info("Пользователи в Keycloak реалме созданы. [Old User ID]-[Keycloak User ID] маппинг: {}",
                userKeycloakIdByOldId);

        logger.info("Обновление данных в БД проекта в соответствии с новыми ID пользователей...");
        transactionTemplate.executeWithoutResult(status -> {
            logger.trace("change 'user_account_id' column in 'users' table");
            jdbcTemplate.batchUpdate("update users set user_account_id = ? where id = ?",
                    new BatchPreparedStatementSetter() {
                        @Override
                        public void setValues(PreparedStatement ps, int i) throws SQLException {
                            Long oldUserId = oldUsers.get(i).getId();
                            ps.setString(1, userKeycloakIdByOldId.get(oldUserId));
                            ps.setLong(2, oldUserId);
                        }

                        @Override
                        public int getBatchSize() {
                            return oldUsers.size();
                        }
                    });
            logger.trace("delete from 'reset_password_tokens'");
            jdbcTemplate.batchUpdate("delete from reset_password_tokens");
        });
        logger.info("Данные в БД проекта обновлены");
    }
}
