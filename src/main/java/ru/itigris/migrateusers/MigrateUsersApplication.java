package ru.itigris.migrateusers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.itigris.migrateusers.app.properties.MigrateProperties;
import ru.itigris.migrateusers.service.KeycloakSdk;
import ru.itigris.migrateusers.service.MigrateService;

import java.util.Scanner;

@SpringBootApplication
public class MigrateUsersApplication implements ApplicationRunner {

    private final Logger logger = LoggerFactory.getLogger(MigrateUsersApplication.class);

    @Autowired
    private MigrateService migrateService;

    @Autowired
    private MigrateProperties migrateProperties;

    public static void main(String[] args) {
        SpringApplication.run(MigrateUsersApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        logger.info("Миграции будут выполнены для проекта {}", migrateProperties.getTargetProject());
        logger.info("Начать миграции? Введите 'y/yes'");
        String answer = scanner.next();
        if ("yes".equals(answer) || "y".equals(answer)) {
            migrateService.migrate();
        }
    }
}
