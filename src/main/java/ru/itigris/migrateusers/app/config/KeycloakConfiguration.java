package ru.itigris.migrateusers.app.config;

import org.keycloak.OAuth2Constants;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.itigris.migrateusers.app.properties.KeycloakProperties;
import ru.itigris.migrateusers.app.properties.MigrateProperties;

/**
 * Конфигурация Keycloak.
 * <p>
 * В основном для работы с реалмом вам понадобится только бин {@link #realm)}.
 */
@Configuration
public class KeycloakConfiguration {

    private final Logger logger = LoggerFactory.getLogger(KeycloakConfiguration.class);

    private final KeycloakProperties keycloakProperties;
    private final MigrateProperties migrateProperties;

    public KeycloakConfiguration(KeycloakProperties keycloakProperties,
                                 MigrateProperties migrateProperties) {
        this.keycloakProperties = keycloakProperties;
        this.migrateProperties = migrateProperties;
    }

    /**
     * Нам нужен данный бин, чтобы Spring Boot мог прочитать параметры Keycloak из application файла.
     *
     * @return KeycloakSpringBootConfigResolver
     * @see <a href="https://issues.redhat.com/browse/KEYCLOAK-11282">Открытый тикет по проблеме</a>
     */
    @Bean
    public KeycloakSpringBootConfigResolver keycloakConfigResolver() {
        return new KeycloakSpringBootConfigResolver();
    }

    @Bean
    public Keycloak keycloak() {
        logger.info("Конфигурация инстанса Keycloak. Auth URL: {}, Реалм: {}",
                keycloakProperties.getAuthServerUrl(), keycloakProperties.getRealm());
        return KeycloakBuilder.builder()
                .serverUrl(keycloakProperties.getAuthServerUrl())
                .realm(keycloakProperties.getRealm())
                .grantType(OAuth2Constants.PASSWORD)
                .clientId("admin-cli")
                .username(migrateProperties.getKeycloakAdmin().getUsername())
                .password(migrateProperties.getKeycloakAdmin().getPassword())
                .build();
    }

    @Bean
    public RealmResource realm(Keycloak keycloak) {
        return keycloak.realm(keycloakProperties.getRealm());
    }
}
