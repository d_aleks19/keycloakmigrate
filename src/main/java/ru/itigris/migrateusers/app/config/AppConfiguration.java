package ru.itigris.migrateusers.app.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import ru.itigris.migrateusers.app.properties.KeycloakProperties;
import ru.itigris.migrateusers.app.properties.MigrateProperties;

import javax.sql.DataSource;

@Configuration
@EnableConfigurationProperties({KeycloakProperties.class, MigrateProperties.class})
public class AppConfiguration {

    private final Logger logger = LoggerFactory.getLogger(AppConfiguration.class);

    // Target project Datasource Configuration goes here..

    @Bean
    @ConfigurationProperties(prefix = "spring.project-datasource")
    public DataSource projectDatasource() {
        logger.info("Конфигурация DataSource для БД Проекта");
        return DataSourceBuilder.create().build();
    }

    @Bean
    public JdbcTemplate projectJdbcTemplate(@Qualifier("projectDatasource") DataSource dataSource) {
        logger.info("Конфигурация JdbcTemplate для БД Проекта");
        return new JdbcTemplate(dataSource);
    }

    @Bean
    DataSourceTransactionManager projectTransactionManager(@Qualifier("projectDatasource") DataSource dataSource) {
        logger.info("Конфигурация TransactionManager для БД Проекта");
        return new DataSourceTransactionManager(dataSource);
    }

    // Keycloak Datasource Configuration goes here..

    @Bean
    @ConfigurationProperties(prefix = "spring.keycloak-datasource")
    public DataSource keycloakDatasource() {
        logger.info("Конфигурация DataSource для БД Keycloak");
        return DataSourceBuilder.create().build();
    }

    @Bean
    public JdbcTemplate keycloakJdbcTemplate(@Qualifier("keycloakDatasource") DataSource dataSource) {
        logger.info("Конфигурация JdbcTemplate для БД Keycloak");
        return new JdbcTemplate(dataSource);
    }

    @Bean
    DataSourceTransactionManager keycloakTransactionManager(@Qualifier("keycloakDatasource") DataSource dataSource) {
        logger.info("Конфигурация TransactionManager для БД Keycloak");
        return new DataSourceTransactionManager(dataSource);
    }
}
