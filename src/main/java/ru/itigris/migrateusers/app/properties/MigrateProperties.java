package ru.itigris.migrateusers.app.properties;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

import java.util.Objects;
import java.util.Optional;

/**
 * Свойства миграций.
 */
@ConstructorBinding
@ConfigurationProperties(prefix = "migrate")
public class MigrateProperties {

    /**
     * Описание credentials админ-пользователя.
     */
    @NestedConfigurationProperty
    @NotNull
    private final KeycloakAdminProperties keycloakAdmin;

    /**
     * Проект, для которого выполняется миграция.
     *
     * @see TargetProject
     */
    @NotNull
    private final TargetProject target;

    public MigrateProperties(
            @NotNull KeycloakAdminProperties keycloakAdmin,
            @NotNull String target) {
        this.keycloakAdmin = Objects.requireNonNull(keycloakAdmin);
        this.target = TargetProject.of(Objects.requireNonNull(target)).orElseThrow(
                () -> new IllegalArgumentException("Неподдерживаемый проект: " + target));
    }

    @NotNull
    public KeycloakAdminProperties getKeycloakAdmin() {
        return keycloakAdmin;
    }

    @NotNull
    public TargetProject getTargetProject() {
        return target;
    }

    public enum TargetProject {

        COOPER("COOPER"),
        WEB_ORDERING("WEB-ORDERING");

        public final String value;

        TargetProject(String value) {
            this.value = value;
        }

        public static Optional<TargetProject> of(String name) {
            for (TargetProject targetProject : values()) {
                if (targetProject.value.equals(name)) {
                    return Optional.of(targetProject);
                }
            }
            return Optional.empty();
        }
    }
}
