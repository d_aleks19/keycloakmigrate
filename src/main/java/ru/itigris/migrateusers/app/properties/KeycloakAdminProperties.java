package ru.itigris.migrateusers.app.properties;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * Credentials админ-пользователя.
 * <p>
 * Админ требуется для создания новых пользователей на реалме (миграции старых пользователей).
 * Убедитесь, что админ-пользователь имеет необходимые права на реалме для создания пользователей.
 */
public class KeycloakAdminProperties {

    @NotNull
    private final String username;

    @NotNull
    private final String password;

    public KeycloakAdminProperties(@NotNull String username, @NotNull String password) {
        this.username = Objects.requireNonNull(username);
        this.password = Objects.requireNonNull(password);
    }

    @NotNull
    public String getUsername() {
        return username;
    }

    @NotNull
    public String getPassword() {
        return password;
    }
}
