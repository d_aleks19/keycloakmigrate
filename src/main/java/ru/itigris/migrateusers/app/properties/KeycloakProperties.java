package ru.itigris.migrateusers.app.properties;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Свойства Keycloak.
 */
@ConstructorBinding
@ConfigurationProperties(prefix = "keycloak")
public class KeycloakProperties {

    @NotNull
    private final String realm;

    @NotNull
    private final String authServerUrl;

    public KeycloakProperties(@NotNull String realm, @NotNull String authServerUrl) {
        this.realm = Objects.requireNonNull(realm);
        this.authServerUrl = Objects.requireNonNull(authServerUrl);
    }

    @NotNull
    public String getRealm() {
        return realm;
    }

    @NotNull
    public String getAuthServerUrl() {
        return authServerUrl;
    }
}
